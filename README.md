# Oracle Extended Grammar

## Installation

```bash 
composer require "blu-platform/oracle-extended-grammar @dev"
```

Once Composer has installed or updated your packages you need to register it. Open up config/app.php and find the providers key and add:

```php
BluPlatform\OracleExtendedGrammar\Providers\OracleExtendedServiceProvider::class,
```