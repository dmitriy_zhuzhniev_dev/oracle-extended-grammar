<?php

namespace BluPlatform\OracleExtendedGrammar\Query\Grammars;

use Yajra\Oci8\Query\Grammars\OracleGrammar;

class OracleExtendedGrammar extends OracleGrammar
{
    //@todo extends OracleGrammar from laravel-oci8

    public function wrapYear($column)
    {
        return "TO_CHAR({$column}, 'YYYY')";
    }

    public function wrapMonth($column)
    {
        return "TO_CHAR({$column}, 'MM')";
    }

    public function wrapDay($column)
    {
        return "TO_CHAR({$column}, 'DD')";
    }

    public function wrapDate($column)
    {
        return "TO_CHAR({$column}, 'YYYY-MM-DD')";
    }
}