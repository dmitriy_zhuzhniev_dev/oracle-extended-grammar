<?php

namespace BluPlatform\OracleExtendedGrammar\Providers;

use BluPlatform\OracleExtendedGrammar\OracleExtendedConnection;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\DatabaseServiceProvider;

class OracleExtendedServiceProvider extends DatabaseServiceProvider
{
    /**
     * Register the primary database bindings.
     *
     * @return void
     */
    protected function registerConnectionServices()
    {
        $this->app->singleton('db.factory', function ($app) {
            return new ConnectionFactory($app);
        });

        $this->app->singleton('db', function ($app) {

            $dbm = new DatabaseManager($app, $app['db.factory']);

            $dbm->extend('oracle', function ($config, $name) use ($app) {
                $connection = $app['db.factory']->make($config, $name);

                return new OracleExtendedConnection(
                    $connection->getPdo(),
                    $connection->getDatabaseName(),
                    $connection->getTablePrefix(),
                    $config
                );
            });

            return $dbm;
        });

        $this->app->bind('db.connection', function ($app) {
            return $app['db']->connection();
        });
    }
}