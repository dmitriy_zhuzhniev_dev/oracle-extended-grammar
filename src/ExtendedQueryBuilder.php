<?php

namespace BluPlatform\OracleExtendedGrammar;

use Yajra\Oci8\Query\OracleBuilder;

class ExtendedQueryBuilder extends OracleBuilder
{
    public function date($column)
    {
        return $this->raw($this->grammar->wrapDate($this->grammar->wrap($column)));
    }

    public function selectYear($column, $slug = null)
    {
        $slug = $slug ?: $this->slugFromColumn($column);

        return $this->selectRaw(
            $this->grammar->wrapYear($this->grammar->wrap($column)) . " as $slug"
        );
    }

    public function selectMonth($column, $slug = null)
    {
        $slug = $slug ?: $this->slugFromColumn($column);

        return $this->selectRaw(
            $this->grammar->wrapMonth($this->grammar->wrap($column)) . " as $slug"
        );
    }

    public function addSelectDay($column, $slug = null)
    {
        $slug = $slug ?: $this->slugFromColumn($column);

        return $this->selectRaw(
            $this->grammar->wrapDay($this->grammar->wrap($column)) . " as $slug"
        );
    }

    public function selectDate($column, $slug = null)
    {
        $slug = $slug ?: $this->slugFromColumn($column);

        return $this->selectRaw(
            $this->grammar->wrapDate($this->grammar->wrap($column)) . " as $slug"
        );
    }

    protected function slugFromColumn($column)
    {
        return last(explode('.', $column));
    }
}