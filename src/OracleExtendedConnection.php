<?php

namespace BluPlatform\OracleExtendedGrammar;

use BluPlatform\OracleExtendedGrammar\Query\Grammars\OracleExtendedGrammar as QueryGrammar;
use BluPlatform\OracleExtendedGrammar\Schema\Grammars\OracleExtendedGrammar as SchemaGrammar;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Connection;

class OracleExtendedConnection extends Connection
{
    public function query() {
        return new ExtendedQueryBuilder(
            $this,
            $this->getQueryGrammar(),
            $this->getPostProcessor()
        );
    }

    public function date($value)
    {
        return new Expression($value);
    }

    protected function getDefaultQueryGrammar()
    {
        return $this->withTablePrefix(new QueryGrammar());
    }

    protected function getDefaultSchemaGrammar()
    {
        return $this->withTablePrefix(new SchemaGrammar());
    }
}